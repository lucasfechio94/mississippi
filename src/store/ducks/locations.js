const Types = {
    ADD_LOCATION: 'locations/ADD_LOCATION',
    UPDATE_LOCATION: 'locations/UPDATE_LOCATION',
}

const initialState = {
    locations: [],
}

export function addLocation(location) {
    return {
        type: Types.ADD_LOCATION,
        payload: location,
    }
}

export function updateLocation(location, index) {
    return {
        type: Types.UPDATE_LOCATION,
        payload: { location, index },
    }
}

export default function reducer(state = initialState, action) {  
    switch (action.type) {
        case Types.ADD_LOCATION:
            return { ...state, locations: [...state.locations, action.payload] }
        case Types.UPDATE_LOCATION:
            return {...state, locations: state.locations.map((elem, index) => {
                if(index === action.payload.index) {
                    return action.payload.location
                }

                return elem
            })}
        default:
            return state
    }
}