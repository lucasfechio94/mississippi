import axios from 'axios'

const instance = axios.create({
    baseURL: 'https://fcc-weather-api.glitch.me'
})

export default function (lat, lon) {
    return instance.get('/api/current', { params: { lat, lon } })
} 