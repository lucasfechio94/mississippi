import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom'
import Home from '../../components/Home'
import Locations from '../../components/Locations'

const Routes = () => (
    <Router>
        <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/locations' component={Locations} />
        </Switch>
    </Router>
)

export default Routes