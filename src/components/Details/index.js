import React, { memo } from 'react'
import PropTypes from 'prop-types'
import { isEmpty, first, get } from 'lodash'
import { ArrowDownward, ArrowUpward } from '@material-ui/icons'

import './style.scss'

const days = [
    'domingo', 'segunda-feira', 'terça-feira', ' quarta-feira', 'quinta-feira', 'sexta-feira', 'sábado'
]

const Details = memo(({ data, loading }) => {
    const isLoading = () => (<span>Carregando dados...</span>)
    const date = new Date()
    
    return (
        <div className='details-container'>
            {
                isEmpty(data)
                    ? (
                        loading
                            ? isLoading()
                            : <span>Selecione uma localidade</span>
                    )
                    : (
                        loading
                            ? isLoading()
                            : (
                                <>
                                    <div className='top-block'>
                                        <h2>{data.name}</h2>
                                        <span>{days[date.getDay()]}, {date.getHours()}:{date.getMinutes()}</span>
                                    </div>
                                    <div className='middle-block'>
                                        <div className='temp-info'>
                                            <img alt='' src={get(first(get(data, 'weather')), 'icon')} />
                                            <h1>{`${get(data, 'main.temp')} °C`}</h1>
                                        </div>
                                        <div className='extra-infos'>
                                            <span>Umidade: {get(data, 'main.humidity')}%</span>
                                            <span>Vento: {get(data, 'wind.speed')} km/h</span>
                                        </div>
                                    </div>
                                    <div className='bottom-block'>
                                        <ArrowDownward className='down-arrow-icon' />
                                        <span>{get(data, 'main.temp_min')} °C</span>
                                        <ArrowUpward className='up-arrow-icon' />
                                        <span>{get(data, 'main.temp_max')} °C</span>
                                    </div>
                                </>
                            )
                    )
            }
        </div>
    )
})

Details.defaultProps = {
    loading: false,
}

Details.propTypes = {
    data: PropTypes.object,
    loading: PropTypes.bool,
}

export default Details