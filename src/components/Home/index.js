import React, { useState, useCallback } from 'react'
import { Link } from 'react-router-dom'
import { get, first } from 'lodash'
import { useDispatch, useSelector } from 'react-redux'
import Details from '../Details'
import Snackbar from '../Snackbar'
import getWeather from '../../gateways/weather'
import { addLocation, updateLocation } from '../../store/ducks/locations'

import './style.scss'

const cities = [
    { id: 1, name: 'Ribeirão Preto', lat: -21.179778, lon: -47.8729702 },
    { id: 2, name: 'São Carlos', lat: -22.0184566, lon: -47.9310767 },
    { id: 3, name: 'São José do Rio Pardo', lat: -21.6056484, lon: -46.9385692 },
]

const Home = () => {
    const dispatch = useDispatch()
    const [data, setData] = useState({})
    const [loading, setLoading] = useState(false)
    const [open, setOpen] = useState(false)
    const { locations } = useSelector(state => {
        return state.locations
    })

    const fetchData = useCallback(async (lat, lon) => {
        setLoading(true)
        try {
            const { data } = await getWeather(lat, lon)
            // essa verificação é feita pois as vezes a api
            // retorna uma localidade diferente (Shuzenji) da lat e lon informadas
            if (data.id === 1851632) {
                fetchData(lat, lon)
            } else {
                setData(data)
                const location = {
                    id: data.id,
                    name: data.name,
                    temp_max: get(data, 'main.temp_max'),
                    temp_min: get(data, 'main.temp_min'),
                    icon: get(first(get(data, 'weather')), 'icon')
                }
                if (locations.some(elem => elem.id === data.id)) {
                    dispatch(updateLocation(location, locations.findIndex(i => i.id === data.id)))
                } else {
                    dispatch(addLocation(location))
                }
                setLoading(false)
            }
        } catch (error) {
            setOpen(true)
            setLoading(false)
        }
    }, [dispatch, locations])

    return (
        <div className='home-container'>
            <div className='buttons-group'>
                {
                    cities.map((city, idx) =>
                        <button key={city.id} onClick={() => fetchData(city.lat, city.lon)}>{city.name}</button>
                    )
                }
            </div>
            <Details
                data={data}
                loading={loading}
            />
            <Link to='/locations'>
                <button>Mostrar Min/Máx</button>
            </Link>
            <Snackbar
                open={open}
                handleClose={() => setOpen(false)}
                message='Erro ao carregar dados'
            />
        </div >
    )
}

export default Home