import React from 'react'
import { isEmpty } from 'lodash'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { Reply } from '@material-ui/icons'

import './style.scss'

const Locations = () => {
    const { locations } = useSelector(state => {
        return state.locations
    })

    return (
        <div className='card-container'>
            <h1>Mínima/Máxima</h1>
            {
                isEmpty(locations)
                    ? <h3> Nenhuma consulta foi feita! </h3>
                    : (
                        locations.map(city =>
                            <div key={city.id} className='card'>
                                <h3>{city.name}</h3>
                                <div className='card-infos'>
                                    <div className='card-temps'>
                                        <span>Mínima: {city.temp_min} °C</span>
                                        <span>Máxima: {city.temp_max} °C</span>
                                    </div>
                                    <img alt='' src={city.icon} />
                                </div>
                            </div>
                        )
                    )
            }
            <Link to='/'>
                <Reply style={{ fontSize: '50px' }} />
            </Link>
        </div>
    )
}

export default Locations