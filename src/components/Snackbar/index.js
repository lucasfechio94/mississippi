import React, { useEffect, memo } from 'react'
import PropTypes from 'prop-types'
import { Close } from '@material-ui/icons'

import './style.scss'

const Snackbar = memo(({ open, message, handleClose }) => {
    useEffect(() => {
        open && setTimeout(function () { handleClose() }, 5000);
    }, [open, handleClose])

    return (
        <div className={open ? 'snackbar show' : 'snackbar'}>
            {message}
            <Close
                className='close-icon'
                onClick={handleClose}
            />
        </div>
    )
})

Snackbar.propTypes = {
    open: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired,
    handleClose: PropTypes.func.isRequired,
}

export default Snackbar