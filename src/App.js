import React from 'react'
import { Provider } from 'react-redux'
import Routes from './shared/routes'
import store from './store'
import './App.scss'

const App = () => (
  <Provider store={store}>
    <div className="App">
      <Routes />
    </div>
  </Provider>
)

export default App;
